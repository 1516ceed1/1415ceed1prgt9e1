package org.ceedcv.ceed1prgt9e1.controlador;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;

import org.ceedcv.ceed1prgt9e1.modelo.IModelo;
import org.ceedcv.ceed1prgt9e1.modelo.ModeloDb4o;
import org.ceedcv.ceed1prgt9e1.modelo.ModeloMysql;
import org.ceedcv.ceed1prgt9e1.vista.VistaAcerca;
import org.ceedcv.ceed1prgt9e1.vista.VistaAlumno;
import org.ceedcv.ceed1prgt9e1.vista.VistaGrupo;
import org.ceedcv.ceed1prgt9e1.vista.VistaGrupoAlumnos;
import org.ceedcv.ceed1prgt9e1.vista.VistaPrincipal;
import org.ceedcv.ceed1prgt9e1.vista.VistaPortada;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Controlador implements ActionListener {

   private IModelo modelo;

   //VistaGrafica vista;
   private JInternalFrame jinternalFrame = null;
   private VistaPrincipal vistaPrincipal = null;
   private VistaGrupo vistaGrupo = null;
   private VistaAlumno vistaAlumno = null;
   private VistaGrupoAlumnos vistaGraficaGrupoAlumnos = null;
   private VistaAcerca vistaAcerca = null;
   private VistaPortada vistaPortada = null;

   private ControladorAlumno controladorAlumno = null;
   private ControladorGrupo controladorGrupo = null;
   private ControladorGrupoAlumnos controladorGrupoAlumnos;
   private ControladorAcerca controladorAcerca;

   final static String TITULO = "Aplicación de Ejemplo. Tema 9. BDOO.";
   final static String ALUMNO = "ALUMNO";
   final static String GRUPO = "GRUPO";
   final static String GRUPOALUMNOS = "GRUPOALUMNOS";
   final static String WEB = "WEB";
   final static String ACERCA = "ACERCA";
   final static String PDF = "PDF";
   final static String INSTALAR = "INSTALAR";
   final static String DESINSTALAR = "DEINSTALAR";
   final static String PORTADA = "PORTADA";
   final static String SALIR = "SALIR";

   Controlador(IModelo modelo_, VistaPrincipal vista_) {
      vistaPrincipal = vista_;
      modelo = modelo_;
      inicializa();
   }

   @Override
   public void actionPerformed(ActionEvent e) {

      String comando = e.getActionCommand();

      switch (comando) {
         case SALIR:
            salir();
            break;
         case ALUMNO:
            alumno();
            break;
         case GRUPO:
            grupo();
            break;
         case GRUPOALUMNOS:
            grupoalumnos();
            break;
         case WEB:
            web();
            break;
         case PDF:
            pdf();
            break;
         case INSTALAR:
            instalar();
            break;
         case DESINSTALAR:
            desinstalar();
            break;
         case ACERCA:
            acerca();
            break;
         case PORTADA:
            portada();
            break;
      }

   }

   private void salir() {
      vistaPrincipal.setVisible(false);
      System.exit(0);
   }

   private void inicializa() {

      vistaPrincipal.getAlumno().setActionCommand(ALUMNO);
      vistaPrincipal.getGrupo().setActionCommand(GRUPO);
      vistaPrincipal.getGrupoAlumnos().setActionCommand(GRUPOALUMNOS);
      vistaPrincipal.getWeb().setActionCommand(WEB);
      vistaPrincipal.getAcerca().setActionCommand(ACERCA);
      vistaPrincipal.getPortada().setActionCommand(PORTADA);
      vistaPrincipal.getInstalar().setActionCommand(INSTALAR);
      vistaPrincipal.getDesinstalar().setActionCommand(DESINSTALAR);
      vistaPrincipal.getPdf().setActionCommand(PDF);
      vistaPrincipal.getSalir().setActionCommand(SALIR);

      vistaPrincipal.getAlumno().addActionListener(this);
      vistaPrincipal.getGrupo().addActionListener(this);
      vistaPrincipal.getGrupoAlumnos().addActionListener(this);
      vistaPrincipal.getWeb().addActionListener(this);
      vistaPrincipal.getAcerca().addActionListener(this);
      vistaPrincipal.getPortada().addActionListener(this);
      vistaPrincipal.getInstalar().addActionListener(this);
      vistaPrincipal.getDesinstalar().addActionListener(this);
      vistaPrincipal.getPdf().addActionListener(this);
      vistaPrincipal.getSalir().addActionListener(this);

      vistaPrincipal.setTitle(TITULO);
      vistaPrincipal.getTitulo().setText(TITULO);
      vistaPrincipal.setLocationRelativeTo(null); // Centrar
      portada();
      vistaPrincipal.setVisible(true);

   }

   private void portada() {

      //vistaPortada = vistaPortada.getInstancia();
      vistaPortada = new VistaPortada();
      vistaPrincipal.getEscritorio().add(vistaPortada);
      vistaPortada.getTitulo().setText(TITULO);
      vistaPortada.setTitle(PORTADA);
      vistaPortada.setVisible(true);
      try {
         vistaPortada.setMaximum(true);
         vistaPortada.setSelected(true);
      } catch (java.beans.PropertyVetoException e) {
      }

   }

   private void acerca() {
      controladorAcerca = new ControladorAcerca(vistaPrincipal);

   }

   private void alumno() {

      controladorAlumno = new ControladorAlumno(vistaPrincipal, modelo);
   }

   private void grupo() {

      controladorGrupo = new ControladorGrupo(vistaPrincipal, modelo);

   }

   private void grupoalumnos() {

      controladorGrupoAlumnos = new ControladorGrupoAlumnos(vistaPrincipal, modelo);
   }

   private void pdf() {
      try {
         String filename = "docu.pdf";
         File doc = new File(filename);
         Desktop.getDesktop().open(doc);
      } catch (IOException ex) {
         Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
      }
   }

   private void web() {

      String url = "https://docs.google.com/document/d/1jyXCoOPyi_9GDJMo0UzUvd2I8oLCVXSwmBJonXnFckA/edit?usp=sharing";

      Desktop desktop = Desktop.getDesktop();

      if (desktop.isSupported(Desktop.Action.BROWSE)) {
         try {
            desktop.getDesktop().browse(new URI(url));
         } catch (URISyntaxException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
         }
      } else {
         pdf();
      }
   }

   private void instalar() {
      modelo.instalar();
   }

   private void desinstalar() {
      modelo.desinstalar();
      if (controladorGrupo != null) {
         controladorGrupo.inicializaGrupos();
         controladorGrupo.visible(false);
      }
      //controladorAlumno.inicializaAlumno();

   }

}
