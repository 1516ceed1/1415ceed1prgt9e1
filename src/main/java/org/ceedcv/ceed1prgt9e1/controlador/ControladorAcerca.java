/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt9e1.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.ceedcv.ceed1prgt9e1.vista.Funciones;
import org.ceedcv.ceed1prgt9e1.vista.VistaAcerca;
import org.ceedcv.ceed1prgt9e1.vista.VistaPrincipal;

/**
 *
 * @author paco
 */
class ControladorAcerca implements ActionListener {

   final static String TITULOVENTANA = "ACERCA";
   final static String ACERCASALIR = "ACERCASALIR";
   private VistaPrincipal vistaPrincipal;
   private VistaAcerca vistaAcerca;

   ControladorAcerca(VistaPrincipal vistaPrincipal) {
      this.vistaPrincipal = vistaPrincipal;
      inicializa();
   }

   private void salir() {
      vistaAcerca.setVisible(false);
   }

   private void inicializa() {

      Funciones f = new Funciones();
      f.cierraFrames(vistaPrincipal);

      vistaAcerca = new VistaAcerca();
      vistaPrincipal.getEscritorio().add(vistaAcerca);

      vistaAcerca.setTitle(TITULOVENTANA);
      vistaAcerca.getjbSalir().setActionCommand(ACERCASALIR);
      vistaAcerca.getjbSalir().addActionListener(this);

      vistaAcerca.setVisible(true);
      try {
         vistaAcerca.setMaximum(true);
         vistaAcerca.setSelected(true);
      } catch (java.beans.PropertyVetoException e) {
      }

   }

   @Override
   public void actionPerformed(ActionEvent e) {
      String comando = e.getActionCommand();

      switch (comando) {
         case ACERCASALIR:
            salir();
            break;
      }
   }

}
